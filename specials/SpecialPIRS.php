<?php
/**
 * pirs SpecialPage for pirs extension
 *
 * @file
 * @ingroup Extensions
 */
class SpecialPIRS extends SpecialPage {
	public function __construct() {
		parent::__construct( 'pirs' );
	}

	/**
	 * Show the page to the user
	 *
	 * @param string $sub The subpage string argument (if any).
	 */
	public function execute( $sub ) {
		$out = $this->getOutput();
		$out->setPageTitle( $this->msg( 'special-pirs-title' ) );
		$out->addHelpLink( 'How to become a MediaWiki hacker' );
		$out->addWikiMsg( 'special-pirs-intro' );
	}

	protected function getGroupName() {
		return 'other';
	}
}
