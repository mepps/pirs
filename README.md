# PIRS



## Getting started

This is the PIRS prototyping repo where we will try out different approaches to PIRS extension development.

## Installation

* Clone/Download the extension and place into /extensions/PIRS
* wfLoadExtension( 'PIRS' ); in LocalSettings.php.
* Click SpecialPages link. Verify PIRS link appears on page.
