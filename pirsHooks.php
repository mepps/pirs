<?php
/**
 * pirs extension hooks
 *
 * @file
 * @ingroup Extensions
 * @license GPL-2.0+
 */
class pirsHooks {
	/**
	 * Conditionally register the unit testing module for the ext.pirs module
	 * only if that module is loaded
	 *
	 * @param array $testModules The array of registered test modules
	 * @param ResourceLoader $resourceLoader The reference to the resource loader
	 * @return true
	 */
	public static function onResourceLoaderTestModules( array &$testModules, ResourceLoader &$resourceLoader ) {
		$testModules['qunit']['ext.pirs.tests'] = [
			'scripts' => [
				'tests/pirs.test.js'
			],
			'dependencies' => [
				'ext.pirs'
			],
			'localBasePath' => __DIR__,
			'remoteExtPath' => 'pirs',
		];
		return true;
	}
}
