<?php
/**
 * Aliases for special pages in the pirs extension.
 */
// @codingStandardsIgnoreFile

$specialPageAliases = array();

/** English (English) */
$specialPageAliases['en'] = array(
	'pirs' => array( 'pirs' ),
);
